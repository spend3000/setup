#!/usr/bin/env bash
#       _                 _
#   ___(_)_ __ ___  _ __ | | ___   _
#  / __| | '_ ` _ \| '_ \| |/ _ \_| |_
#  \__ \ | | | | | | |_) | |  __/_   _|
#  |___/_|_| |_| |_| .__/|_|\___| |_|
#                  |_|
#
# Boilerplate for creating a simple bash script with some basic strictness
# checks, help features, easy debug printing.
#
# Usage:
#   bash-simple-plus argument
#
# Depends on:
#  list
#  of
#  programs
#  expected
#  in
#  environment
#
# Bash Boilerplate: https://github.com/alphabetum/bash-boilerplate
#
# Copyright (c) 2015 William Melody • hi@williammelody.com

# Notes #######################################################################

# Extensive descriptions are included for easy reference.
#
# Explicitness and clarity are generally preferable, especially since bash can
# be difficult to read. This leads to noisier, longer code, but should be
# easier to maintain. As a result, some general design preferences:
#
# - Use leading underscores on internal variable and function names in order
#   to avoid name collisions. For unintentionally global variables defined
#   without `local`, such as those defined outside of a function or
#   automatically through a `for` loop, prefix with double underscores.
# - Always use braces when referencing variables, preferring `${NAME}` instead
#   of `$NAME`. Braces are only required for variable references in some cases,
#   but the cognitive overhead involved in keeping track of which cases require
#   braces can be reduced by simply always using them.
# - Prefer `printf` over `echo`. For more information, see:
#   http://unix.stackexchange.com/a/65819
# - Prefer `$_explicit_variable_name` over names like `$var`.
# - Use the `#!/usr/bin/env bash` shebang in order to run the preferred
#   Bash version rather than hard-coding a `bash` executable path.
# - Prefer splitting statements across multiple lines rather than writing
#   one-liners.
# - Group related code into sections with large, easily scannable headers.
# - Describe behavior in comments as much as possible, assuming the reader is
#   a programmer familiar with the shell, but not necessarily experienced
#   writing shell scripts.

###############################################################################
# Strict Mode
###############################################################################

# Treat unset variables and parameters other than the special parameters ‘@’ or
# ‘*’ as an error when performing parameter expansion. An 'unbound variable'
# error message will be written to the standard error, and a non-interactive
# shell will exit.
#
# This requires using parameter expansion to test for unset variables.
#
# http://www.gnu.org/software/bash/manual/bashref.html#Shell-Parameter-Expansion
#
# The two approaches that are probably the most appropriate are:
#
# ${parameter:-word}
#   If parameter is unset or null, the expansion of word is substituted.
#   Otherwise, the value of parameter is substituted. In other words, "word"
#   acts as a default value when the value of "$parameter" is blank. If "word"
#   is not present, then the default is blank (essentially an empty string).
#
# ${parameter:?word}
#   If parameter is null or unset, the expansion of word (or a message to that
#   effect if word is not present) is written to the standard error and the
#   shell, if it is not interactive, exits. Otherwise, the value of parameter
#   is substituted.
#
# Examples
# ========
#
# Arrays:
#
#   ${some_array[@]:-}              # blank default value
#   ${some_array[*]:-}              # blank default value
#   ${some_array[0]:-}              # blank default value
#   ${some_array[0]:-default_value} # default value: the string 'default_value'
#
# Positional variables:
#
#   ${1:-alternative} # default value: the string 'alternative'
#   ${2:-}            # blank default value
#
# With an error message:
#
#   ${1:?'error message'}  # exit with 'error message' if variable is unbound
#
# Short form: set -u
set -o nounset

# Exit immediately if a pipeline returns non-zero.
#
# NOTE: this has issues. When using read -rd '' with a heredoc, the exit
# status is non-zero, even though there isn't an error, and this setting
# then causes the script to exit. read -rd '' is synonymous to read -d $'\0',
# which means read until it finds a NUL byte, but it reaches the EOF (end of
# heredoc) without finding one and exits with a 1 status. Therefore, when
# reading from heredocs with set -e, there are three potential solutions:
#
# Solution 1. set +e / set -e again:
#
# set +e
# read -rd '' variable <<EOF
# EOF
# set -e
#
# Solution 2. <<EOF || true:
#
# read -rd '' variable <<EOF || true
# EOF
#
# Solution 3. Don't use set -e or set -o errexit at all.
#
# More information:
#
# https://www.mail-archive.com/bug-bash@gnu.org/msg12170.html
#
# Short form: set -e
set -o errexit

# Print a helpful message if a pipeline with non-zero exit code causes the
# script to exit as described above.
trap 'echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2' ERR

# Allow the above trap be inherited by all functions in the script.
#
# Short form: set -E
set -o errtrace

# Return value of a pipeline is the value of the last (rightmost) command to
# exit with a non-zero status, or zero if all commands in the pipeline exit
# successfully.
set -o pipefail

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
IFS=$'\n\t'

###############################################################################
# Environment
###############################################################################

# $_ME
#
# Set to the program's basename.
_ME=$(basename "${0}")

###############################################################################
# Debug
###############################################################################

# _debug()
#
# Usage:
#   _debug printf "Debug info. Variable: %s\n" "$0"
#
# A simple function for executing a specified command if the `$_USE_DEBUG`
# variable has been set. The command is expected to print a message and
# should typically be either `echo`, `printf`, or `cat`.
__DEBUG_COUNTER=0
_debug() {
  if [[ "${_USE_DEBUG:-"0"}" -eq 1 ]]
  then
    __DEBUG_COUNTER=$((__DEBUG_COUNTER+1))
    # Prefix debug message with "bug (U+1F41B)"
    printf "🐛  %s " "${__DEBUG_COUNTER}"
    "${@}"
    printf "――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――\\n"
  fi
}
# debug()
#
# Usage:
#   debug "Debug info. Variable: $0"
#
# Print the specified message if the `$_USE_DEBUG` variable has been set.
#
# This is a shortcut for the _debug() function that simply echos the message.
debug() {
  _debug echo "${@}"
}

###############################################################################
# Die
###############################################################################

# _die()
#
# Usage:
#   _die printf "Error message. Variable: %s\n" "$0"
#
# A simple function for exiting with an error after executing the specified
# command. The command is expected to print a message and should typically
# be either `echo`, `printf`, or `cat`.
_die() {
  # Prefix die message with "cross mark (U+274C)", often displayed as a red x.
  printf "❌  "
  "${@}" 1>&2
  exit 1
}
# die()
#
# Usage:
#   die "Error message. Variable: $0"
#
# Exit with an error and print the specified message.
#
# This is a shortcut for the _die() function that simply echos the message.
die() {
  _die echo "${@}"
}

###############################################################################
# Help
###############################################################################

# _print_help()
#
# Usage:
#   _print_help
#
# Print the program help information.
_print_help() {
  cat <<HEREDOC
     _                 _
 ___(_)_ __ ___  _ __ | | ___   _
/ __| | '_ \` _ \\| '_ \\| |/ _ \\_| |_
\\__ \\ | | | | | | |_) | |  __/_   _|
|___/_|_| |_| |_| .__/|_|\\___| |_|
                |_|

Spend initial setup

Usage:
  ${_ME} [--options] [<arguments>]
  ${_ME} -h | --help
         -e <value>
         --debug

Options:
  -h --help  Display this help information.
  -e         Choose test or prod environment
  --debug
HEREDOC
}

###############################################################################
# Options
#
# NOTE: The `getops` builtin command only parses short options and BSD `getopt`
# does not support long arguments (GNU `getopt` does), so the most portable
# and clear way to parse options is often to just use a `while` loop.
#
# For a pure bash `getopt` function, try pure-getopt:
#   https://github.com/agriffis/pure-getopt
#
# More info:
#   http://wiki.bash-hackers.org/scripting/posparams
#   http://www.gnu.org/software/libc/manual/html_node/Argument-Syntax.html
#   http://stackoverflow.com/a/14203146
#   http://stackoverflow.com/a/7948533
#   https://stackoverflow.com/a/12026302
#   https://stackoverflow.com/a/402410
###############################################################################

# Parse Options ###############################################################

# Initialize program option variables.
_PRINT_HELP=0
_USE_DEBUG=0

# Initialize additional expected option variables.
_ENV="prod"

# _require_argument()
#
# Usage:
#   _require_argument <option> <argument>
#
# If <argument> is blank or another option, print an error message and  exit
# with status 1.
_require_argument() {
  # Set local variables from arguments.
  #
  # NOTE: 'local' is a non-POSIX bash feature and keeps the variable local to
  # the block of code, as defined by curly braces. It's easiest to just think
  # of them as local to a function.
  local _option="${1:-}"
  local _argument="${2:-}"

  if [[ -z "${_argument}" ]] || [[ "${_argument}" =~ ^- ]]
  then
    _die printf "Option requires a argument: %s\\n" "${_option}"
  fi
}

while [[ ${#} -gt 0 ]]
do
  __option="${1:-}"
  __maybe_param="${2:-}"
  case "${__option}" in
    -h|--help)
      _PRINT_HELP=1
      ;;
    --debug)
      _USE_DEBUG=1
      ;;
    -e)
      _require_argument "${__option}" "${__maybe_param}"
      _ENV="${__maybe_param}"
       shift
      ;;
    --endopts)
      # Terminate option parsing.
      break
      ;;
    -*)
      _die printf "Unexpected option: %s\\n" "${__option}"
      ;;
  esac
  shift
done

###############################################################################
# Program Functions
###############################################################################

_run() {
  _PIP_PATH='/usr/bin/pip3'

  printf ">> Spend install\\n"

  if [[ $_ENV != "test" && $_ENV != "prod" ]]
  then
    _die "Env is test or prod"
  fi

  printf ">>Env : %s\\n" "${_ENV}\\n\\nThis program setup Spend environment on Armbian.\\n"
  printf "Required : spend user created, with spend package in its home folder\\n"

  printf "Apt update...\\n"
  apt-get update -y -qq > /dev/null

  _prepare_virtual_env

  _install_packages
  _add_users
  _tweak_configuration
  _install_spend
}

# Install virtualenv, create one and activate it
_prepare_virtual_env() {

  _debug printf "\nPreparing virtual env"

  apt-get install -y -qq virtualenv > /dev/null
  sudo -u spend virtualenv -p python3 /home/spend/.spendenv
  sudo -u spend virtualenv -p python3 /home/spend/spend_update

  _PIP_PATH='/home/spend/.spendenv/bin/pip3'
  # Virtualenv has an old bug with bash strict mode https://github.com/pypa/virtualenv/issues/1029
  _activate_virtualenv
}

_activate_virtualenv () {
  set +u
  . /home/spend/.spendenv/bin/activate
  set -u
}

_install_packages() {

  printf "\n\nInstalling packages\n"

  apt-get install -y -qq python3-dev gpgconf gpg debhelper build-essential pmount i2c-tools > /dev/null

  # New root password generation. Removed later
  apt-get install -y -qq whois > /dev/null

  # Display output
  apt-get install -y -qq zlib1g-dev libfreetype6-dev libjpeg-dev > /dev/null


  # Install usbmount
  _debug printf "\nInstalling Usbmount from sources"

  mkdir -p /tmp/usbmount
  wget -q https://github.com/rbrito/usbmount/archive/master.zip -O /tmp/usbmount/master.zip > /dev/null
  unzip /tmp/usbmount/master.zip -d /tmp/usbmount/master > /dev/null

  cd /tmp/usbmount/master/usbmount-master
  dpkg-buildpackage -us -uc -b > /dev/null
  apt-get install -y -qq ../usbmount_0.0.24_all.deb > /dev/null

  # Install ssd1306 lib for i2c control
  sudo -u spend -H ${_PIP_PATH} install git+https://github.com/codelectron/ssd1306.git
  # ini configuration management
  sudo -u spend -H ${_PIP_PATH} install git+https://github.com/DiffSK/configobj.git


  _debug printf "\n\n Installing zuluCrypt-cli from apt"
  apt-get install -y -qq zulucrypt-cli > /dev/null

  # Compile zuluCrypt
  #_debug printf "\n\n Installing zuluCrypt-cli from sources"

  #sudo apt-get install -y -qq libblkid-dev gcc g++ cmake libcryptsetup-dev libgcrypt11-dev pkg-config libdevmapper-dev uuid-dev libudev-dev chrpath bzip2 debhelper > /dev/null

  #mkdir -p /tmp/zulu
  #wget -q https://github.com/mhogomchungu/zuluCrypt/archive/master.zip -O /tmp/zulu/master.zip > /dev/null
  #unzip /tmp/zulu/master.zip -d /tmp/zulu/master > /dev/null
  #cd /tmp/zulu/master/zuluCrypt-master

  #mkdir build
  #cd build

  #printf "\ncompiling zuluCrypt. Can take a long time."

  #cmake -DCMAKE_INSTALL_PREFIX=/usr -DSHARE_MOUNT_PREFIX=default -DUDEVSUPPORT=true -DNOGUI=true -DQT5=false -DHOMEMOUNTPREFIX=false -DREUSEMOUNTPOINT=false -DNOGNOME=false -DINTERNAL_ZULUPLAY=true -DNOKDE=true -DINTERNAL_LXQT_WALLET=true -DUSE_POLKIT=false -DCMAKE_BUILD_TYPE=RELEASE . .. > /dev/null
  #make > /dev/null
  #make install > /dev/null

}

_add_users() {
  _debug printf "Adding users"
  addgroup --system zulucryptusers

  adduser spend i2c
  adduser spend zulucryptusers

  addgroup --system gpio
  usermod -aG gpio spend
}

_tweak_configuration() {
  _debug printf "Tweaking configuration"

  # Adds ~7s to systemd boot time
  systemctl disable NetworkManager-wait-online

  # Allow zulucrypt be executed as root (needed for volume creation)
  # @deprecated Execution is now done by root
  echo "%zulucryptusers ALL=(root) NOPASSWD:/home/<USER>/Documents/spend/tests/test_volume.py, /usr/bin/zuluCrypt-cli" | sudo tee /etc/sudoers.d/zulucrypt
  chmod 0440 /etc/sudoers.d/zulucrypt

  echo  "export SPEND_ENV=${_ENV}" >> /home/spend/.bashrc
  export SPEND_ENV=$_ENV
  source "/home/spend/.bashrc"
  # Add reset shred/crypt file to bashrc ?

  printf "\n\nNow activate i2c in the kernel : overlays should contain i2c0\n"
  read -r -p "Press enter to continue"
  nano /boot/armbianEnv.txt

  # Permissions
  printf "\n\nPlease set that : "
  echo "Mount options: Options passed to the mount command with the -o flag."
  echo "See the warning above regarding removing sync from the options."
  printf "MOUNTOPTIONS=\"uid=1000,gid=1000,sync,noexec,nodev,noatime,nodiratime\"\n"
  read -r -p "Press enter to continue"
  nano /etc/usbmount/usbmount.conf
}

_install_spend() {
    printf "\n\nInstalling Spend\n\n"

    # Update directory
    mkdir -p /home/spend/update

    # Global config
    mkdir -p /etc/spend

    # Custom config
    mkdir -p /home/spend/.spend

    # Create environ file, used by services, and install package
    environ_file_content=""

    if [[ $_ENV != "test" ]]
    then
      environ_file_content="HOME=/home/spend
PATH=/home/spend/.spendenv/bin:/usr/local/bin:/usr/bin:/bin
SPEND_ENV=prod
VIRTUAL_ENV=\"/home/spend/.spendenv\"
"

      ${_PIP_PATH} install /home/spend/spend

    else
      environ_file_content="HOME=/home/spend
PATH=/home/spend/.spendenv/bin:/usr/local/bin:/usr/bin:/bin
SPEND_ENV=test
VIRTUAL_ENV=\"/home/spend/.spendenv\"
"

      ${_PIP_PATH} install -e /home/spend/spend[dev]

    fi

    echo "$environ_file_content" > /home/spend/.spend/environ

    echo "1" > /home/spend/.crypt.state
    echo "1" > /home/spend/.shred.state

    chown spend: /home/spend/.crypt.state
    chown spend: /home/spend/.shred.state
    chown -R spend: /home/spend/.spend
    chown -R spend: /home/spend/update
}

###############################################################################
# Main
###############################################################################

# _main()
#
# Usage:
#   _main [<options>] [<arguments>]
#
# Description:
#   Entry point for the program, handling basic option parsing and dispatching.
_main() {
  if ((_PRINT_HELP))
  then
    _print_help
  else

    if [[ "$(id -u)" != "0" ]]; then
       echo "This script must be run as root" 1>&2
       exit 1
    fi

    _run "$@"
    exit 0
  fi
}

# Call `_main` after everything has been defined.
_main "$@"
