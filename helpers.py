import logging
from distutils.dir_util import copy_tree


def get_random_str(size):
    """Return x chars belonging to letters and digits."""
    import random
    import string

    poll = string.ascii_letters + string.digits
    return ''.join(random.choice(poll) for i in range(size))


def copy_anything(src, dst):
    import shutil
    import errno
    try:
        copy_tree(src, dst)
    except OSError as exc:
        if exc.errno == errno.ENOTDIR:
            shutil.copy(src, dst)
        else:
            raise


# def copytree(src, dst, symlinks=False, ignore=None):
#     from os import listdir, path
#     from shutil import copy2
#     from shutil import copytree as ogcopytree
#
#     for item in listdir(src):
#         s = path.join(src, item)
#         d = path.join(dst, item)
#         if path.isdir(s):
#             ogcopytree(s, d, symlinks, ignore)
#         else:
#             copy2(s, d)


def write_file(path, content):
    with open(path, 'w+') as f:
        logging.info('Writing file %s', path)
        f.write(content)
        f.close()
