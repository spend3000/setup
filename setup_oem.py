#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import logging
from glob import glob
from os import path
from subprocess import check_call

from configobj import ConfigObj

import helpers


class SpendSetup:
    CONF_SECTION = 'oem'

    gpg_client = None

    def __init__(self, env, sd_mountpoint, keyrings_path):
        self.env = env

        self.conf = ConfigObj('conf.ini')

        self.spend_id = self.get_spend_id()

        self.mountpoint = sd_mountpoint

        self.keyrings_path = path.join(keyrings_path, 'unit-%s' % self.spend_id)

    def setup(self):
        """
        Setup device
        :return:
        """
        print('Initial image should have been flashed.\nGoing to copy gpg wallet and setup files to the target µSd\n')

        if input("Type 'go' to start script\n") == 'go':

            if 'test' != self.env:
                with open(path.join(self.keyrings_path, 'client', '.fingapt')) as fd:
                    fingapt = fd.read()
                    fd.close()

                logging.info('Fingapt : %s', fingapt)
                self._adapt_config_files(fingapt)

            self.copy_gpg_info(self.keyrings_path)

            self.increment_spend_id()

    @staticmethod
    def get_spend_id(new_id=None):
        """
        Return value in file spend_id if no arg specified, else write new id and return its value
        :param new_id:int
        :return: int
        """
        with open('spend_id', 'r+') as f:
            if new_id is not None:
                f.write(str(new_id))
                returned = new_id
            else:
                returned = f.read()
            f.close()
            return int(returned)

    def copy_gpg_info(self, keyring_path):
        """Copy .gpg directory to the device

        :return:
        """
        print("Copying gpg info")

        helpers.copy_anything(
            path.join(keyring_path, self.conf[SpendSetup.CONF_SECTION]['device_keyring_dir']),
            path.join(self.mountpoint,
                      self.conf[SpendSetup.CONF_SECTION]['spend_homedir'], self.conf['device']['gpg_src'])
        )

        helpers.copy_anything(
            path.join(keyring_path,
                      self.conf[SpendSetup.CONF_SECTION]['client_keyring_dir']),
            path.join(
                self.mountpoint,
                self.conf[SpendSetup.CONF_SECTION]['spend_homedir'],
                'DEFAULT_KEYRING_TO_REMOVE')
        )

    # @todo: should we use spend-update instead ?
    def _adapt_config_files(self, fingapt):
        """Update config file to the new recipient

        :param fingapt: str Key fingerprint to import
        :return:
        """

        print('Adapting setting recipient fingerprint %s in config files :' % fingapt)

        custom_config_dir = path.join(
            self.mountpoint,
            self.conf[SpendSetup.CONF_SECTION]['spend_homedir'],
            self.conf['device']['spend_user_data'])

        custom_config_files = glob(path.join(custom_config_dir, '*.ini'))

        for _path in custom_config_files:
            if not path.islink(_path):
                logging.info('Gonna update config %s', _path)

                parser = ConfigObj(_path)
                parser['VOLUME']['recipient'] = fingapt

                with open(_path, 'br+') as configfile:
                    parser.write(configfile)

    def increment_spend_id(self):
        """
        Increment spend_id
        :return:
        """
        self.get_spend_id(self.spend_id + 1)

    def test(self):
        keyfile = glob(path.join(self.mountpoint, '*.key'))
        container = glob(path.join(self.mountpoint, '*.files'))

        if len(keyfile) > 1 or len(container) > 1:
            raise RuntimeError('More than one key or container found')
        elif not len(keyfile) or not len(container):
            raise RuntimeError('Keyfile or container not found')

        command = 'zuluCrypt-cli -o -M -m test -t vera -e rw -d {container} ' \
                  '-p $(gpg --homedir {gpghome} -a --pinentry-mode loopback --passphrase-file={keyringpasspath} --yes -d {key})' \
                  ' && zuluCrypt-cli -q -d {container}'

        formatted_command = command.format(
            container=container.pop(), key=keyfile.pop(),
            keyringpasspath=path.join(self.keyrings_path, self.conf[SpendSetup.CONF_SECTION]['client_keyring_dir'],
                                      'TO_REMOVE.passphrase'),
            gpghome=path.join(self.keyrings_path, self.conf[SpendSetup.CONF_SECTION]['client_keyring_dir']))

        logging.info(formatted_command)

        check_call(formatted_command, shell=True)


if __name__ == "__main__":
    # Command line options and help
    argparser = argparse.ArgumentParser(
        description='Spend installer script, used on a third-part device to create Spend device.')
    argparser.add_argument('-e', '--environment', choices=['test', 'prod'], help='Spend environment', required=True)
    argparser.add_argument('-m', '--mountpoint', help='Spend sd card mountpoint', required=True, action='store')
    argparser.add_argument('-k', '--keyrings', help='Keyring directory path.', required=True, action='store')
    argparser.add_argument('action', metavar='action', type=str, choices=['setup', 'test'], help='Action requested')
    argparser.add_argument(
        '-q', '--quiet', action='store_true',
        help='Set logging level to INFO. Default to DEBUG', required=False)
    args = argparser.parse_args()

    loglevel = logging.INFO if args.quiet else logging.DEBUG
    logging.basicConfig(
        level=loglevel, format='[%(levelname)s] %(filename)s %(message)s')
    # level=loglevel, format='[%(levelname)s] %(filename)s (%(msecs)s) %(message)s',)

    s = SpendSetup(args.environment, args.mountpoint, args.keyrings)

    print('  ____     ____   U _____ u _   _    ____')
    print(' / __"| uU|  _"\ u\| ___"|/| \ |"|  |  _"\\')
    print('<\___ \/ \| |_) |/ |  _|" <|  \| |>/| | | |')
    print(' u___) |  |  __/   | |___ U| |\  |uU| |_| | \\')
    print(' |____/>> |_|      |_____| |_| \_|  |____/ u')
    print('  )(  (__)||>>_    <<   >> ||   \\,-.|||_')
    print(' (__)    (__)__)  (__) (__)(_")  (_/(__)_)')
    print("**********************************************")
    print("******* OEM INSTALLER [{!s}] [v{!s}] ********* \n".format(
        args.environment, s.conf['DEFAULT']['version']))

    if args.action == 'setup':
        s.setup()
    elif args.action == 'test':
        s.test()
    else:
        raise RuntimeError('Action is test or setup')
