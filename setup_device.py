#!/usr/bin/env python

import argparse
import logging
from os import chmod, chown
from os.path import join
from shutil import rmtree

from configobj import ConfigObj

from helpers import get_random_str, write_file


def inplace_change(filename, old_string, new_string):
    # Safely read the input filename using 'with'
    with open(filename) as f:
        s = f.read()
        if old_string not in s:
            raise RuntimeError('"{old_string}" not found in {filename}.'.format(**locals()))

    # Safely write the changed content, if found in the file
    with open(filename, 'w') as f:
        print('Changing "{old_string}" to "{new_string}" in {filename}'.format(**locals()))
        s = s.replace(old_string, new_string)
        f.write(s)


def change_root_password(default_hash_root, default_hash_spend, home_dir):
    # noinspection PyUnresolvedReferences
    from sh import mkpasswd
    logging.info('Changing root password')

    npass = get_random_str(10)
    encnpass = str(mkpasswd('-m', 'sha-512', npass))

    # TODO: when process tested, remove this debug
    logging.error("new root password is : %s and its encrypted version : %s", npass, encnpass)

    pass_file = join(home_dir, 'ROOT_PASSWORD_TO_SAVE_AND_DELETE.txt')

    write_file(pass_file, npass)
    chown(pass_file, 1000, 1000)

    inplace_change('/etc/shadow', default_hash_root.rstrip(), encnpass.rstrip())
    inplace_change('/etc/shadow', default_hash_spend.rstrip(), encnpass.rstrip())


if __name__ == "__main__":
    # Command line options and help
    argparser = argparse.ArgumentParser(
        description='Spend installer script, used on a third-part device to create Spend device.')
    argparser.add_argument(
        '-q', '--quiet', action='store_true',
        help='Set logging level to INFO. Default to DEBUG', required=False)
    args = argparser.parse_args()

    loglevel = logging.INFO if args.quiet else logging.DEBUG
    logging.basicConfig(
        level=loglevel, format='[%(levelname)s] %(filename)s %(message)s')
    # level=loglevel, format='[%(levelname)s] %(filename)s (%(msecs)s) %(message)s',)

    print(r'  ____     ____   U _____ u _   _    ____')
    print(r' / __"| uU|  _"\ u\| ___"|/| \ |"|  |  _"\\')
    print(r'<\___ \/ \| |_) |/ |  _|" <|  \| |>/| | | |')
    print(r' u___) |  |  __/   | |___ U| |\  |uU| |_| | \\')
    print(r' |____/>> |_|      |_____| |_| \_|  |____/ u')
    print(r'  )(  (__)||>>_    <<   >> ||   \\,-.|||_')
    print(r' (__)    (__)__)  (__) (__)(_")  (_/(__)_)')
    print("**********************************************")

    conf = ConfigObj('conf.ini')

    logging.info('Cleanup bash history')
    with open('/home/spend/.bash_history', 'w'):
        pass

    change_root_password(default_hash_spend=conf['device']['default_hash_spend'],
                         default_hash_root=conf['device']['default_hash_root'], home_dir=conf['device']['home_dir'])

    try:
        chmod('/home/spend/.gnupg/gpg.conf', 600)
    except FileNotFoundError:
        pass

    rmtree(join(conf['device']['home_dir'], conf['device']['spend_src']))
    rmtree(join(conf['device']['home_dir'], 'spend_setup'))
